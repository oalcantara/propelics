﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropelicsTest
{
    class Program
    {
        /// <summary>
        /// Main thread, it implements the Test class to manage the core functionality
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Test test = new Test();

            test.processMenu();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PropelicsTest
{
    class Test
    {
        /// <summary>
        /// Manages the main menu and calls to all the related methods
        /// </summary>
        public void processMenu()
        {
            printMenu();
            switch (readMenuOption())
            {
                case 1:
                    computeFormula();
                    break;
                case 2:
                    calculateAngleBetweenClockArms();
                    break;
                case 3:
                    checkLeapYear();
                    break;
                case 4:
                    ConverToDecimal();
                    break;
                case 5:
                    // For this excercise, the value is read here because the instructions indicate
                    // that the function must receive the number of pairs as a parameter.
                    short pairs;
                    System.Console.Write("Enter the number of parentheses pairs: ");
                    while (!Int16.TryParse(System.Console.ReadLine(), out pairs))
                    {
                        System.Console.WriteLine("Invalid number");
                    }
                    AllParenthesesCombinations(pairs);
                    break;
                case 6:
                    MatrixOfZeroes();
                    break;
                case 7:
                    StringPermutations();
                    break;
                case 8:
                    System.Console.WriteLine("Exiting.");
                    break;
                default:
                    System.Console.WriteLine("Invalid option.");
                    break;
            }

        }

        /// <summary>
        /// Reads and applies a basic validation to the option that the user wants to execute
        /// </summary>
        /// <returns>A short value containing the chosen option</returns>
        private short readMenuOption()
        {
            short option;
            while (!Int16.TryParse(System.Console.ReadLine(), out option) || option<1 || option>8)
            {
                System.Console.WriteLine("Invalid option");
            }

            return option;
        }

        /// <summary>
        /// Prints the main menu
        /// </summary>
        private void printMenu()
        {
            System.Console.WriteLine("Choose test question:\n");
            System.Console.WriteLine("1. Compute a formula ");
            System.Console.WriteLine("2. Clock inner angle between hour and minute");
            System.Console.WriteLine("3. Leap years");
            System.Console.WriteLine("4. Decimal conversion");
            System.Console.WriteLine("5. Parentheses combinations");
            System.Console.WriteLine("6. Matrix of zeroes");
            System.Console.WriteLine("7. String permutations");
            System.Console.WriteLine("8. Exit\n");
            System.Console.Write("Enter an option number: ");
        }

        /// <summary>
        /// Executes the computation of the formula for the first excercise
        /// </summary>
        private void computeFormula()
        {
            double totalLeft = 0;
            double totalRight = 0;
            bool add = true;
            int count = 1;

            // We process the left side of the equation and show the result
            System.Console.WriteLine("Processing left side of equation");
            for (int k = 1; k <= 1000000; k++)
            {
                totalLeft += Math.Pow(-1, k + 1) / ((2 * k) - 1);
                if(k%100000==0) System.Console.Write(".");
            }
            totalLeft *= 4;
            System.Console.WriteLine("\nDone! R=" + totalLeft);

            // We process the right side of the equation and stop until the result matches the left side
            System.Console.WriteLine("Processing right side of equation");
            for (int i = 1; true; i = i + 2)
            {
                totalRight += (add ? 1.0 / i : -(1.0/i));
                add = !add;

                count++;
                if(count%100000==0) System.Console.Write(".");

                if(totalRight*4 == totalLeft)
                {
                    System.Console.WriteLine("\nDone! R=" + 4*totalRight + "\nLast number on right side of equation was " + i);
                    System.Console.ReadKey();
                    break;
                }
            }
        }

        /// <summary>
        /// Implements the calculation of the angle between the hour and minute of an analogic clock
        /// </summary>
        private void calculateAngleBetweenClockArms()
        {
            // Read the hour and minute
            short hour;
            System.Console.Write("Enter hour: ");
            while (!Int16.TryParse(System.Console.ReadLine(), out hour) || hour < 1 || hour > 12)
            {
                System.Console.WriteLine("Invalid hour");
            }
            short minute;
            System.Console.Write("Enter minute: ");
            while (!Int16.TryParse(System.Console.ReadLine(), out minute) || minute < 0 || minute > 59)
            {
                System.Console.WriteLine("Invalid minute");
            }

            // Calculates the angle
            double hDegrees = (hour * 30) + (minute * 30.0 / 60);
            double mDegrees = minute * 6;
            double diff = Math.Abs(hDegrees - mDegrees);
            if (diff > 180) diff = 360 - diff;
            Console.WriteLine("The angle between the hands is {0} degrees", diff);

            System.Console.ReadKey();
        }

        /// <summary>
        /// Validates if a specific year is a leap year
        /// Note that the excercise has a mistake on the definition of a leap year, as it says "not by 400"
        /// Reference: https://en.wikipedia.org/wiki/Leap_year
        /// </summary>
        private void checkLeapYear()
        {
            // Read the year to validate
            int year;
            System.Console.Write("Enter year: ");
            while (!Int32.TryParse(System.Console.ReadLine(), out year))
            {
                System.Console.WriteLine("Invalid year");
            }

            // Calculate and print the result
            System.Console.WriteLine("The year {0} is {1} leap year.", year, ((year % 100 == 0 && year % 400 == 0) || (year % 4 == 0 && year%100!=0)) ? "in deed a" : "not a");
            System.Console.ReadKey();
        }

        /// <summary>
        /// Converts a base n number into decimal base
        /// </summary>
        private void ConverToDecimal()
        {
            string number;
            short _base;
            int lenght;
            int value = 0;

            // Read the number and base
            System.Console.Write("Enter number: ");
            number= System.Console.ReadLine().Trim().ToUpper();
            System.Console.Write("Enter base [2, 8, 16]: ");
            while (!Int16.TryParse(System.Console.ReadLine(), out _base) || (_base!=2 && _base!=8 && _base!=16 && _base!=10))
            {
                System.Console.WriteLine("Invalid base");
                System.Console.ReadKey();
            }

            // If base is 10, no transformation is required
            if (_base==10)
            {
                System.Console.WriteLine("Number can not be convertes to decimal, it's already base 10.");
                System.Console.ReadKey();
            }
            else
            {
                // Make basic validations to input number according to its base
                Regex r=new Regex(@"^[0-9]*$");
                if (_base == 2) { r = new Regex(@"^[0-1]*$"); }
                if (_base == 8) { r = new Regex(@"^[0-8]*$"); }
                if (_base == 16) { r = new Regex(@"^[A-Fa-f0-9]*$"); }
                if (!r.IsMatch(number))
                {
                    System.Console.WriteLine("Invalid number for that base.");
                    System.Console.ReadKey();
                    return;
                }

                // Make the transformation to decimal
                lenght = number.Length;
                foreach(Char digit in number)
                {
                    int numDigit = 0;
                    if (_base==16)
                    {
                        if (digit == 'A') numDigit = 10;
                        if (digit == 'B') numDigit = 11;
                        if (digit == 'C') numDigit = 12;
                        if (digit == 'D') numDigit = 13;
                        if (digit == 'E') numDigit = 14;
                        if (digit == 'F') numDigit = 15;
                        if (numDigit == 0) numDigit = (int)Char.GetNumericValue(digit);
                    }
                    else
                    {
                        numDigit = (int)Char.GetNumericValue(digit);
                    }
                    value += numDigit * (int)Math.Pow(_base, --lenght);
                }

                // Print the result
                System.Console.WriteLine("The number converted to base 10 is " + value);
                System.Console.ReadKey();
            }
        }

        /// <summary>
        /// Displays all combinations of properly open and closed parentheses based on the desired number of pairs
        /// This is a wrapper function to validate minimum pairs, create the array and make a nested call to the corresponding function
        /// </summary>
        /// <param name="pairs">Number of pairs to combine</param>
        private void AllParenthesesCombinations(short pairs)
        {
            if (pairs > 0)
            {
                char[] str = new char[2 * pairs];
                PrintParentheses(str, 0, pairs, 0, 0);
            }
            else
            {
                System.Console.WriteLine("Invalid number of pairs requested.");
            }
            System.Console.ReadKey();
        }

        // Print all combinations of balanced parentheses 
        // open store the count of opening parentheses 
        // close store the count of closing parentheses 
        private void PrintParentheses(char[] str, int pos, int pairs, int open, int close)
        {
            if (close == pairs)
            {
                // print the possible combinations 
                for (int i = 0; i < str.Length; i++)
                    Console.Write(str[i]);

                Console.WriteLine();
                return;
            }
            else
            {
                if (open > close)
                {
                    str[pos] = ')';
                    PrintParentheses(str, pos + 1, pairs, open, close + 1);
                }
                if (open < pairs)
                {
                    str[pos] = '(';
                    PrintParentheses(str, pos + 1, pairs, open + 1, close);
                }
            }
        }

        /// <summary>
        /// Creates a dynamic size matrix with user defined values and when it finds a zero it
        /// creates a modified matrix where the full row and column are set to zero
        /// </summary>
        private void MatrixOfZeroes()
        {
            // Read the size of the matrix and create it
            short rows = ReadShort("Enter the number of Rows on the matrix: ");
            short cols = ReadShort("Enter the number of Columns on the matrix: ");
            bool zeroesFound = false;

            int[,] matrix = new int[rows,cols];

            // Read the values for all elements of the matrix
            for (short i = 0; i < rows; i++)
            {
                for (short j = 0; j < cols; j++)
                {
                    matrix[i, j] = ReadShort("Enter value for row " + i + ", column " + j + " of the matrix: ");
                    if (matrix[i, j] == 0) zeroesFound = true;
                }
            }

            System.Console.WriteLine("The original matrix is as follows: ");
            PrintMatrix(matrix);

            // If there are zeroes, then a new matrix is created to replace the full row and column with zeroes
            if (!zeroesFound)
            {
                System.Console.WriteLine("There are no zeroes in this matrix, no other operation is needed");
            }
            else
            {
                System.Console.WriteLine("Press a key to continue...");
                System.Console.ReadKey();
                System.Console.WriteLine("The modified matrix looks as follows: ");
                int[,] newMatrix = new int[rows, cols];
                Array.Copy(matrix, newMatrix, rows*cols);

                // We go through the matrix to find zeroes and set its entire row and column to zero
                for (short i = 0; i < rows; i++)
                {
                    for (short j = 0; j < cols; j++)
                    {
                        if (matrix[i, j] == 0)
                            SetRowAndColToZero(newMatrix, i, j);
                    }
                }

                //Finally we print the modified matrix
                PrintMatrix(newMatrix);
            }

            System.Console.ReadKey();
        }

        /// <summary>
        /// Loops through the matrix and prints its values
        /// </summary>
        /// <param name="matrix"></param>
        private void PrintMatrix(int[,] matrix)
        {
            for (short i = 0; i < matrix.GetLength(0); i++)
            {
                for (short j = 0; j < matrix.GetLength(1); j++)
                {
                    System.Console.Write(matrix[i, j] + "\t");
                }
                System.Console.WriteLine("");
            }
        }

        /// <summary>
        /// Sets the specified row and column of the input matrix to zero
        /// </summary>
        /// <param name="matrix">The matrix of values to operate</param>
        /// <param name="row">Row that will be set to 0</param>
        /// <param name="col">Column that will be set to 0</param>
        private void SetRowAndColToZero(int[,] matrix, short row, short col)
        {
            //We set the entire row to zero
            for(short i = 0; i < matrix.GetLength(1); i++)
            {
                matrix[row, i] = 0;
            }

            //We set the entire column to zero
            for (short i = 0; i < matrix.GetLength(1); i++)
            {
                matrix[i, col] = 0;
            }

        }

        /// <summary>
        /// Reads a string and gets its permutations 
        /// </summary>
        private void StringPermutations()
        {
            System.Console.Write("Enter string to permutate: ");
            string str = System.Console.ReadLine();

            char[] strArray = str.ToCharArray();
            GetPermutations(strArray, 0, str.Length-1);

            System.Console.ReadKey();
        }

        // Function to get all string permutations recursively
        private void GetPermutations(char[] str, int start, int end)
        {
            // If the full string was already parsed, we print the resulting permutation
            // otherwise, we keep swaping characters.
            if (start == end)
            {
                System.Console.WriteLine(str);
            }
            else
            {
                for (int i = start; i <= end; i++)
                {
                    Swap(ref str[start], ref str[i]);
                    GetPermutations(str, start + 1, end);
                    Swap(ref str[start], ref str[i]);
                }
            }
        }

        /// <summary>
        /// Swaping with bitwise XOR
        /// </summary>
        /// <param name="a">First char to swap</param>
        /// <param name="b">Second char to swap</param>
        private void Swap(ref char a, ref char b)
        {
            if (a == b) return;

            a ^= b;
            b ^= a;
            a ^= b;
        }

        /// <summary>
        /// Function to read a short value with basic validation
        /// Note: in a production program, the value readings should be done through a function, in this
        /// program some of them were read directly in the functions for time sake.
        /// </summary>
        /// <param name="message">Input message to show to the user</param>
        /// <returns>Input value from the user</returns>
        private short ReadShort(string message)
        {
            short input;
            System.Console.Write(message);
            while (!Int16.TryParse(System.Console.ReadLine(), out input))
            {
                System.Console.WriteLine("Invalid number");
                System.Console.Write(message);
            }

            return input;
        }
    }
}
